import java.util.*;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

import scala.Tuple2;

/*
 * For details on how to setup a Spark project through your IDE or how to create a project jar check
 * https://spark.apache.org/docs/latest/quick-start.html
 * 
 */

public class Benchmarks {
  public static void main(String[] args) {
    SparkConf conf = new SparkConf().setAppName("Simple Application");//.setMaster("local[2]");
    JavaSparkContext sc = new JavaSparkContext(conf);
    if(args[0].equals("1")){
    	String filename = args[2];
    	topkImdb(sc,Integer.parseInt(args[1]), filename);
    }
    else if(args[0].equals("2")){
    	String filename = args[3];
    	treeTop(sc,Integer.parseInt(args[1]), Integer.parseInt(args[2]), filename);
    }
    else if(args[0].equals("3")){
    	String filename = args[2];
    	sortBy(sc,Integer.parseInt(args[1]), filename);
    }
    else if(args[0].equals("4")){
    	String filename = args[2];
    	sortList(sc,Integer.parseInt(args[1]), filename);
    }
  }
  
  public static void treeTop(JavaSparkContext sc, int par, int depth, String filename){
	  	JavaRDD<String> rdd = sc.textFile("/"+filename,par);
	  	
	  	long start = System.currentTimeMillis();
	    JavaRDD<PriorityQueue<Tuple2<Float, String>>> rdd3 = 
	    		rdd.mapPartitions(new FlatMapFunction<Iterator<String>, PriorityQueue<Tuple2<Float, String>>>(){

					@Override
					public Iterable<PriorityQueue<Tuple2<Float, String>>> call(
							Iterator<String> t) throws Exception {
						// TODO Auto-generated method stub

				    	PriorityQueue<Tuple2<Float, String>> result = new PriorityQueue<Tuple2<Float, String>>(K);
				    	while(t.hasNext()){
							String line = t.next();
							Tuple2<Float, String> t1 = new Tuple2<Float, String>(getWeighedRank(line), buildPayload(line, 1));
							if (result.size() < K){
								result.offer(t1);
							}
							else if (result.peek()._1 < t1._1){
								result.poll();
								result.offer(t1);
							}
						}
				    	LinkedList<PriorityQueue<Tuple2<Float, String>>> l = new LinkedList<PriorityQueue<Tuple2<Float, String>>>();
				    	l.add(result);
				    	return l;
					}
	    			
	    		});
	    rdd3.treeReduce(new Function2<PriorityQueue<Tuple2<Float, String>>,
				PriorityQueue<Tuple2<Float, String>>, PriorityQueue<Tuple2<Float, String>>>(){

					@Override
					public PriorityQueue<Tuple2<Float, String>> call(
							PriorityQueue<Tuple2<Float, String>> a,
							PriorityQueue<Tuple2<Float, String>> b)
							throws Exception {
						// TODO Auto-generated method stub
						for (Tuple2<Float, String> elem : b) {

							if (a.size() < K)
								a.offer(elem);
							else if (elem._1 > a.peek()._1) {
								a.poll();
								a.offer(elem);
							}
						}

						return a;
					}}, depth);
	    long stop = System.currentTimeMillis();
	    System.out.println("treeTop Exec time: " + (stop-start));

  }
	    
	    
  
  public static void sortList(JavaSparkContext sc, int par, String filename){
	  JavaRDD<String> rdd = sc.textFile("/"+filename,par);
	  	long start = System.currentTimeMillis();
	    JavaRDD<LinkedList<Tuple2<Float, String>>> rdd3 = rdd.mapPartitions(new FlatMapFunction<Iterator<String>, LinkedList<Tuple2<Float, String>>>(){

	    @Override
	    public Iterable<LinkedList<Tuple2<Float, String>>> call(Iterator<String> t)
	        throws Exception {
	      // TODO Auto-generated method stub
	      LinkedList<Tuple2<Float, String>> lines = new LinkedList<Tuple2<Float, String>>();
	      while(t.hasNext()){    	
	        String s = t.next();
	        Tuple2<Float, String> t1 = new Tuple2<Float, String>(getWeighedRank(s),s);
	        lines.add(t1);
	      }
	      Collections.sort(lines, new IncomeComparator());
	      LinkedList<LinkedList<Tuple2<Float, String>>> list = new LinkedList<LinkedList<Tuple2<Float, String>>>();
	      list.add(lines);
	      return list;
	    }
	      
	    });
	    rdd3.reduce(new Function2<LinkedList<Tuple2<Float, String>>, LinkedList<Tuple2<Float, String>>, LinkedList<Tuple2<Float, String>>>(){

	    @Override
	    public LinkedList<Tuple2<Float, String>> call(
				LinkedList<Tuple2<Float, String>> a,
				LinkedList<Tuple2<Float, String>> b) throws Exception {
	      // TODO Auto-generated method stub
	      LinkedList<Tuple2<Float, String>> result = new LinkedList<Tuple2<Float, String>>();
	      while (a.size() > 0 && b.size() > 0) {

	        if (a.getFirst()._1.compareTo(b.getFirst()._1) <= 0)
	          result.add(a.poll());
	        else
	          result.add(b.poll());
	      }

	      while (a.size() > 0)
	        result.add(a.poll());

	      while (b.size() > 0)
	        result.add(b.poll());
	      
	      return result;
	      
	    }
	      
	    });	    
	    long end = System.currentTimeMillis();
	    System.out.println("MapPartitions: " + (end - start));
  }
  
  public static void topkImdb(JavaSparkContext sc, int par, String filename){
	  	JavaRDD<String> rdd = sc.textFile("/"+filename,par);
	  	
	  	long start = System.currentTimeMillis();
	    JavaRDD<PriorityQueue<Tuple2<Float, String>>> rdd3 = 
	    		rdd.mapPartitions(new FlatMapFunction<Iterator<String>, PriorityQueue<Tuple2<Float, String>>>(){

					@Override
					public Iterable<PriorityQueue<Tuple2<Float, String>>> call(
							Iterator<String> t) throws Exception {
						// TODO Auto-generated method stub

				    	PriorityQueue<Tuple2<Float, String>> result = new PriorityQueue<Tuple2<Float, String>>(K);
				    	while(t.hasNext()){
							String line = t.next();
							Tuple2<Float, String> t1 = new Tuple2<Float, String>(getWeighedRank(line), buildPayload(line, 1));
							if (result.size() < K){
								result.offer(t1);
							}
							else if (result.peek()._1 < t1._1){
								result.poll();
								result.offer(t1);
							}
						}
				    	LinkedList<PriorityQueue<Tuple2<Float, String>>> l = new LinkedList<PriorityQueue<Tuple2<Float, String>>>();
				    	l.add(result);
				    	return l;
					}
	    			
	    		});
	    rdd3.reduce(new Function2<PriorityQueue<Tuple2<Float, String>>,
				PriorityQueue<Tuple2<Float, String>>, PriorityQueue<Tuple2<Float, String>>>(){

					@Override
					public PriorityQueue<Tuple2<Float, String>> call(
							PriorityQueue<Tuple2<Float, String>> a,
							PriorityQueue<Tuple2<Float, String>> b)
							throws Exception {
						// TODO Auto-generated method stub
						for (Tuple2<Float, String> elem : b) {

							if (a.size() < K)
								a.offer(elem);
							else if (elem._1 > a.peek()._1) {
								a.poll();
								a.offer(elem);
							}
						}

						return a;
					}});
	    long stop = System.currentTimeMillis();
	    System.out.println("topk Imdb Exec time: " + (stop-start));
}
  
  public static void sortBy(JavaSparkContext sc, int par, String filename){
	    JavaRDD<String> rdd = sc.textFile("/"+filename,par);
	  	long start = System.currentTimeMillis();
	    rdd.sortBy(new Function<String, Float>(){

	        @Override
	            public Float call(String v1) throws Exception {
	                  // TODO Auto-generated method stub
	              return getWeighedRank(v1);   
	            }
	    }, true, par).collect();
	    long end = System.currentTimeMillis();
	    System.out.println("SortBy: " + (end - start));
  }

  public static class IncomeComparator implements Comparator<Tuple2<Float, String>> {

    @Override
    public int compare(Tuple2<Float, String> a, Tuple2<Float, String> b) {
      return a._1.compareTo(b._1);
    }

  }
  

  
  
  public static String buildPayload(String line, int times) {
		if (times == 1)
			return line;

		String s = line;
		for (int k = 0; k < times - 1; k++)
			s += line;

		return s;
	}
  
  public static Float getWeighedRank(String line) {

		// this is just to avoid line.split(" ")
		// which will create many string tokens

		int count = 0;
		int start = -1;
		boolean prevIsSpace = true;
		float numOfVotes = 0;
		float rank = 0f;
		for (int i = 0; i < line.length(); i++) {

			if (line.charAt(i) == ' ') {

				if (start > 0) {
					if (count == 2) {
						numOfVotes = Float.parseFloat(line.substring(start, i));
						start = -1;
					} else {
						rank = Float.valueOf(line.substring(start, i));
						break;
					}
				}
				prevIsSpace = true;

			} else {

				if (prevIsSpace) {
					count += 1;
					if (count >= 2)
						start = i;
				}

				prevIsSpace = false;
			}
		}

		/*
			weighted rank = (v/(v+k))*X + (k/(v+k))*C

			where:

			X = average for the movie (mean)
			v = number of votes for the movie
			k = minimum votes required to be listed in the top 250 (currently 25000)
			C = the mean vote across the whole report (currently 6.90)
		*/
		rank = (numOfVotes / (numOfVotes + MIN_NUM_OF_VOTES)) * rank
				+ (MIN_NUM_OF_VOTES / (numOfVotes + MIN_NUM_OF_VOTES)) * AVG_RANK;

		return rank;
	}
  
  	public final static int K = 6262 * 10; // 10% of N
	public final static float AVG_RANK = 6.90f;
	public final static int MIN_NUM_OF_VOTES = 25000;  
}

