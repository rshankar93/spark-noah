# Spark-Noah
This project contains tries to improve the aggregation efficiency of jobs within Spark.

In order to use Noah, you must set the "noahFile" system property to point to an existing file that contains can be used to send data to Noah nodes. The expected data format can be found in **noah.txt** located within the base directory of the project.

Please see [here][1] for instructions on building Spark. Additional dependencies(such as the noah jar) have been added to the **pom.xml** file.

The benchmarks file can also be found in the project base directory.

[1]: https://spark.apache.org/docs/latest/building-spark.html
